# Опишите своими словами как работает цикл forEach.

Цикл forEach перебирает массив подобно циклу for.
Выполняет callback-функцию (это функция, которая должна быть выполнена после того, как другая
функция завершила выполнение, она передается в качестве аргумента другой функции) для каждого
элемента массива по очереди.

То есть цикл forEach это функция у которой в качестве аргумента идет другая функция (callback),
которая выполняется сначала. У которой в свою очередь так же есть аргументы - elements (текущий
обрабатываемый элемент в массиве), index (индекс текущего обрабатываемого элемента в массиве) и
array (массив, по которому осуществляется проход). И как в цикле for перебираются по очереди
все элементы массива.

массив.forEach(function (elements, index, array) {
  array[index] = elements.toUpperCase() // выведет последовательно все элементы массива в верхнем регистре
})

