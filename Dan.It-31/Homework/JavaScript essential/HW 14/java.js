$(document).ready(function (){

  $(`.page-section`).on("click", "a", function (event){
    event.preventDefault();
    let id  = $(this).attr('href'),
      top = $(id).offset().top;
    $('body,html').animate({scrollTop: top}, 1500);
  });

  let btn = $('#button-up');
  $(window).scroll(function() {
    if ($(window).scrollTop() < document.documentElement.clientHeight) {
      btn.hide();
    } else {
      btn.show();
    }
  });
  btn.on('click', function() {
    $('html, body').animate({scrollTop:0}, 1500);
  });

  $(`.visibility-section-2`).on("click", function (event) {
  $(`.section-2`).slideToggle()
  })
});




