const img = document.querySelectorAll(`div img`);
const array = [...img];
let counter = 0;
let launched;

let fragment = new DocumentFragment();

const stop = document.createElement(`div`);
stop.classList.add(`stop`)
stop.innerText = `Прекратить`

const start = document.createElement(`div`);
start.classList.add(`start`)
start.innerText = `Возобновить показ`

fragment.append(stop, start);
document.body.append(fragment)

stop.addEventListener(`click`, () => {
  clearInterval(showing)
  launched = false;
})

start.addEventListener(`click`, () => {
  if(launched === false){
    showing = setInterval(viewImages, 3000);
    launched = true;
  }
})

let showing = setInterval(viewImages, 3000);
launched = true;
function viewImages (){
  array[counter].className = 'hide';
  counter +=  1;
  if (counter >= array.length) {
    counter = 0;
  }
  array[counter].className = 'show';
}
