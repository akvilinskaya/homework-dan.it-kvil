const tabs = document.querySelector(`.section-two-menu`);
const tabsContent = document.querySelectorAll(`.section-two-menu-content`);
const tabsTitle = document.querySelectorAll(`.section-two-menu-item-bt`);

tabs.addEventListener(`click`, function (event){
  const li = event.target;
  tabsTitle.forEach(function (elem){
    elem.classList.remove(`active`);
  })
  li.classList.add(`active`);

  tabsContent.forEach( function (element){
    if (element.id === li.innerHTML){
      element.style.display = 'inline-block';
    } else {
      element.style.display = 'none';
    }
  })
})





const loadMore = document.querySelector(`.section-four-gallery-btn`);
const toggleContent = document.querySelectorAll(`.toggle-content`);
const allImg = document.querySelectorAll(`.card`);
const tabsSectionFour = document.querySelector(`.section-four-menu`);
const tabsTitleFour = document.querySelectorAll(`.section-four-menu-item-bt`);
const graphic = document.querySelectorAll(`.Graphic`);
const web = document.querySelectorAll(`.Web`);
const landing = document.querySelectorAll(`.Landing`);
const product = document.querySelectorAll(`.Product`);

const hideImage = function (reverse) {
  for (let i = 0; i < reverse.length; i++) {
    if (i >= 12) {
      reverse[i].classList.add("show")
      loadMore.addEventListener(`click`, function () {
        reverse[i].classList.remove("show")
          loadMore.classList.add(`section-four-gallery-btn-visibility-none`)
          loadMore.classList.remove(`section-four-gallery-btn-visibility`)
      })
    } else if (reverse.length < 12) {
      loadMore.classList.add(`section-four-gallery-btn-visibility-none`)
      loadMore.classList.remove(`section-four-gallery-btn-visibility`)
    } else {
      loadMore.classList.add(`section-four-gallery-btn-visibility`)
      loadMore.classList.remove(`section-four-gallery-btn-visibility-none`)
    }
  }
}

hideImage(toggleContent)

const selectionImages = function ( target, section) {
  target.classList.add(`active-color-btn`);

  section.forEach(function (element){
    element.hidden = false;
    element.classList.remove("show")
    hideImage(section)
  })
}

tabsSectionFour.addEventListener(`click`, function (event){
  const liFour = event.target;
  tabsTitleFour.forEach(function (elem){
    elem.classList.remove(`active-color-btn`);
  })
  liFour.classList.add(`active-color-btn`);

  allImg.forEach(function (e) {
    e.hidden = true;
    if (liFour.innerHTML === `All`) {
      e.hidden = false;
      hideImage(toggleContent)
    } else if (liFour.innerHTML === `Graphic Design`){
      selectionImages(liFour, graphic)
    } else if (liFour.innerHTML === `Web Design`){
      selectionImages(liFour, web)
    } else if (liFour.innerHTML === `Landing Pages`){
      selectionImages(liFour, landing)
    } else if (liFour.innerHTML === `Product Design`){
      selectionImages(liFour, product)
    }
  })
})






new Swiper(`.image-swiper-big`, {

  thumbs: {
    swiper: {
      el: `.image-swiper`,
      slidesPerView: 4,
    },
  },

  navigation: {
    nextEl: `.section-six-carousel-collection-btn-next`,
    prevEl:`.section-six-carousel-collection-btn-prev`
  },
});

new Swiper(`.swiper-object`, {

  navigation: {
    nextEl: `.section-six-carousel-collection-btn-next`,
    prevEl:`.section-six-carousel-collection-btn-prev`
  },

  slidesPerView: 4,

  slideToClickedSlide: true,

  spaceBetween: 38,
});
