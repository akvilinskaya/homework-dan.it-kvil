const div = document.createElement(`div`);

const label = document.createElement(`label`);
const input = document.createElement(`input`);
label.innerText = `Price, $`

label.setAttribute(`for`, `price`);
input.setAttribute(`type`, `number`);
input.setAttribute(`id`, `price`);
input.setAttribute(`placeholder`, `Enter your price`);

label.style.cssText = `
  margin: 22px;`
div.style.cssText = `
  padding-top: 10px;
  padding-bottom: 10px;
  background-color: beige;
  width: 300px;
  margin-top: 7px;
  `

const fragment = new DocumentFragment();
div.prepend(label, input);
fragment.prepend(div);
document.body.prepend(fragment);
const error = document.createElement(`span`);
document.body.append(error)


const priceIconText = document.createElement(`span`);
const priceIcon = document.createElement(`div`);
const closeCross = document.createElement(`div`);
priceIcon.prepend(priceIconText)
priceIcon.append(closeCross)
document.body.prepend(priceIcon)

const clearError = function (){
  return error.innerHTML = ``;
}

const clearPriceIconText = function (){
  return priceIconText.innerHTML = ``;
}

const deleteInvalid = function (){
  return input.classList.remove('invalid');
}

const deleteSpanPrice = function (){
  return priceIcon.classList.remove('span-price');
}

const deleteCross = function (){
  return closeCross.classList.remove('cross');
}

const clearCloseCross = function (){
  return closeCross.innerHTML = ``;
}

const addFocus = function (){
  return input.classList.add('focus');
}

const deleteFocus = function (){
  return input.classList.remove('focus');
}

const deleteCorrectPrice = function (){
  return input.classList.remove('correct-price');
}

input.onblur = function() {
  deleteFocus();
  if (+this.value <= 0 && +this.value <= -0) {
    this.classList.add('invalid');
    error.innerHTML = 'Please enter correct price!';
    clearPriceIconText();
    deleteSpanPrice();
    deleteCross();
    clearCloseCross();
    deleteCorrectPrice();
  } else if (+this.value > 0){
    deleteInvalid();
    clearError();
    priceIconText.innerHTML = `Текущая цена: ${input.value}`;
    priceIcon.classList.add('span-price');
    closeCross.classList.add('cross');
    closeCross.innerHTML = `&times;`
    this.classList.add('correct-price');
    addFocus();
  }
  if (this.value === ``){
    clearError();
  }
};

input.onfocus = function() {
  addFocus();
  deleteCorrectPrice();
  if (this.value === ``){
    clearError();
  }
  if (this.classList.contains('invalid')) {
    deleteInvalid();
    clearPriceIconText();
    deleteSpanPrice();
    deleteCross();
    clearCloseCross();
  }
};

closeCross.onclick = function (){
  clearPriceIconText();
  deleteSpanPrice();
  deleteCross();
  clearCloseCross();
  input.value = ``;
  deleteFocus();
}
