window.addEventListener('load', () => {


  const order = {
    customer: {
      name: null,
      phone: null
    },
    orderItems: [],
    additional: [],
    coupons: [{
      name: '',
      price: 0.01
    }],
    totalPrice: 1,
  }

  const BASE_TICKET_PRICE = 100;
  const VIP_TICKET_PRICE = 200;
  const ticket = document.querySelector(`#main`);


  ticket.addEventListener(`click`, function (event){
    if (event.target.tagName === `BUTTON` && event.target.classList.contains("vipBtn") === false) {
      event.target.classList.toggle(`btn-color`)
      let getTicet = {
        place: event.target.innerText,
        row: event.target.parentElement.dataset.row,
        isVIP: false,
        price: BASE_TICKET_PRICE,
      };


      order.orderItems.forEach(function (key){
        //console.log(key)
       // console.log(event.target.innerText)
        
        if(order.orderItems.key.includes(event.target.innerText)){
          const index = order.orderItems.indexOf(key);
          order.orderItems.splice(index, 1)
        } else {
          order.orderItems.push(getTicet);
        }

          //
      })

      // if(key.place === event.target.innerText ){
      //   const index = order.orderItems.indexOf(key);
      //   order.orderItems.splice(index, 1)
      // } else {
      //   order.orderItems.push(getTicet);
      // }


      // for (let key of order.orderItems) {
      //   if(key.place === event.target.innerText ){
      //    const index = order.orderItems.indexOf(key);
      //    order.orderItems.splice(index, 1)
      //   }
      // }
      // if(key.place === event.target.innerText){
      //   break
      // } else {
      //   order.orderItems.push(getTicet);
      //   console.log(order)
      // }



    } else if (event.target.tagName === `BUTTON` && event.target.classList.contains("vipBtn") === true){
      event.target.classList.toggle(`vip-button`)
      event.target.classList.toggle(`btn-color`)
      let getTicetVip = {
        place: event.target.innerText,
        row: event.target.parentElement.dataset.row,
        isVIP: true,
        price: VIP_TICKET_PRICE,
      };
      order.orderItems.push(getTicetVip);
    }
    console.log(order)
  })

  const food = document.querySelector(`#form2`);

  food.addEventListener('change', function (event){
    const next = event.target.nextElementSibling
    if (event.target.checked && event.target.classList.contains("check")){
      next.nextElementSibling.classList.remove(`hidden`)
    } else {
      next.nextElementSibling.classList.add(`hidden`)
    }


  })

  food.addEventListener('change', function (event){
    if (event.target.checked && event.target.parentElement.classList.contains("food")){
      const getFood = {
        name: event.target.name,
        size: event.target.nextElementSibling.innerText,
        price: parseInt(event.target.value)
      }
      order.additional.push(getFood);
    }
  })

  const form1 = document.querySelector(`#form1`);

  form1.addEventListener(`input`, function (event){
    if (event.target.getAttribute(`id`) === `name`){
        order.customer.name = event.target.value;
    }
    if (event.target.getAttribute(`id`) === `tel`){
      order.customer.phone =  event.target.value;
    }
    const transferEventName = new CustomEvent(`transferName`, {
      detail: {nameUser: order.customer.name,
        telephone: order.customer.phone,
      }
    })
    document.body.dispatchEvent(transferEventName)
  })


  const orderKino = document.querySelector(`#formBtn`);

  document.body.addEventListener(`transferName`, function (event){
    console.log(event.detail)
    if (event.detail.nameUser.length !== 0 && event.detail.telephone.length !== 0) {
      orderKino.removeAttribute('disabled');
    } else {
      orderKino.setAttribute('disabled', 'disabled');
    }
  })



})


///  const chek = function (mark, menu) {
//     if(mark.checked) {
//       menu.classList.remove(`hidden`)
//     } else {
//       menu.classList.add(`hidden`)
//     }
//   }
//
//   const chekPurchases = document.querySelectorAll(`.check`)
//   console.log(chekPurchases)
//   for( let elem of chekPurchases){
//     console.log(elem)
//
//     chek(elem, next)
//   }
